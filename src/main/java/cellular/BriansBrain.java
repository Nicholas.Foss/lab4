package cellular;

import java.util.ArrayList;
import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

public class BriansBrain extends GameOfLife{
    public BriansBrain(int rows, int columns) {
        super(rows, columns);
	}

@Override
public CellState getNextCell(int row, int col) {
    int deadNeighbors = countNeighbors(row, col, CellState.DEAD);
    int livingNeighbors = countNeighbors(row, col, CellState.ALIVE);

    if (currentGeneration.get(row, col) == CellState.ALIVE){
        return CellState.DYING;
    }
    else if (currentGeneration.get(row, col) == CellState.DYING){
        return CellState.DEAD;
    }
    else{
        if(livingNeighbors == 2){
            return CellState.ALIVE;
        }
        else{ return CellState.DEAD;}
    }
}
}