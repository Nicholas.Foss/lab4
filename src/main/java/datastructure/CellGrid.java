package datastructure;

import java.util.Arrays;

import cellular.CellState;

public class CellGrid implements IGrid {
    int rows;
    int columns;
    CellState[][] cellState;

    public CellGrid(int rows, int columns, CellState initialState) {
		// TODO Auto-generated constructor stub
        this.rows = rows;
        this.columns = columns;
        this.cellState = new CellState[rows][columns];
        for(int r = 0; r < rows; r++){
            for(int c = 0; c < columns; c++){
                cellState[r][c] = initialState;
            }
        }
	}

    @Override
    public int numRows() {
        // TODO Auto-generated method stub
        return this.rows;
    }

    @Override
    public int numColumns() {
        // TODO Auto-generated method stub
        return this.columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
        // TODO Auto-generated method stub
        if(row < 0 || row >= this.rows){
            throw new IndexOutOfBoundsException();
        }
        else if(column < 0 || column >= this.columns){
            throw new IndexOutOfBoundsException();
        }
        this.cellState[row][column] = element;

    }

    @Override
    public CellState get(int row, int column) {
        // TODO Auto-generated method stub
        if(row < 0 || row >= this.rows){
            throw new IndexOutOfBoundsException();
        }
        else if(column < 0 || column >= this.columns){
            throw new IndexOutOfBoundsException();
        }
        return this.cellState[row][column];
    }

    @Override
    public IGrid copy() {
        CellGrid cellStatesCopy = new CellGrid(this.rows, this.columns, CellState.DEAD);
        for(int r = 0; r < this.rows; r++){
            for(int c = 0; c < this.columns; c++){
                CellState currentState = this.get(r,c);
                cellStatesCopy.set(r, c, currentState);
            }
        }
        // TODO Auto-generated method stub
        return cellStatesCopy; //cellStatesCopy;
    }
    
}
